import bus from './bus';

export default {
  install(Vue) {
    Vue.prototype.$alert = (config) => {
      bus.$emit('addAlert', config);
    };
    Vue.$alerts = alert;
  },
};
