export const AlertTypes = {
  Primary: 'primary',
  Warning: 'warning',
  Success: 'success',
  Danger: 'danger',
};

export const AlertPositions = {
  Top: 'top',
  Right: 'right',
  Left: 'left',
  Bottom: 'bottom',
};
