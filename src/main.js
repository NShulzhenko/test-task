import Vue from 'vue';
import VueBootstrap from 'bootstrap-vue';
import App from './App.vue';
import router from './router';
import Alerts from './plugins/alert';

import './scss/index.scss';

Vue.config.productionTip = false;
Vue.use(VueBootstrap);
Vue.use(Alerts);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
