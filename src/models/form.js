// TODO: I could create a class for a field - but it would be kind of overhead for this task

export default Object.assign({}, {
  input1: {
    id: 3,
    type: 'text-field',
    label: 'Input1',
    value: '',
    placeholder: 'Input1',
  },
  input2: {
    id: 4,
    type: 'text-field',
    label: 'Input2',
    value: '',
    placeholder: 'Input2',
  },
  checkbox1: {
    id: 1,
    type: 'checkbox-field',
    label: 'Checkbox1',
    value: false,
    inline: true,
  },
  checkbox2: {
    id: 2,
    type: 'checkbox-field',
    label: 'Checkbox2',
    value: false,
    inline: true,
  },
});
