import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'form',
      // lazy-loading demo
      component: () => import('./components/Form.vue'),
    },
    {
      path: '/alerts',
      name: 'alerts',
      component: () => import('./components/AlertsDemo.vue'),
    },
    {
      path: '/export/:type?',
      name: 'export',
      component: () => import('./components/Export.vue'),
    },
  ],
});
